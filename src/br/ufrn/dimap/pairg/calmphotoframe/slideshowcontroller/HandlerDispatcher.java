package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller;

import android.os.Handler;

public class HandlerDispatcher implements Dispatcher{
	private Handler handler;
	public HandlerDispatcher(Handler handler){
		this.handler = handler;
	}
	@Override
	public void execute(Runnable runnable) {
		handler.post(runnable);
	}

}
