package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller;

import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.AccelerometerGestureListener;
import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.Direction;
import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.GestureController;
import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.gesture.AccelerometerGesture;
import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.gesture.ChangeDirectionGesture;
import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.gesture.HoldDirectionGesture;
import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.gesture.SequenceGesture;

public class SlideshowGestureController implements AccelerometerGestureListener {
	public enum SlideshowCommands{Next, Previous, Flip, Back, Refresh}

	private static final long DEFAULT_DIRECTION_TRANSITION_TIME = 1000L;
	private GestureController gestureController;
	
	public void start(GestureController gestureController){
		this.gestureController = gestureController;
		addGestures(gestureController);
		gestureController.addGestureListener(this);
	}
	
	public void stop(){
		if(gestureController != null){
			removeGestures(gestureController);
		}
	}
	/* *************************** AccelerometerGestureListener *****************************/
	@Override
	public void onGestureDetected(AccelerometerGesture gesture) {
		if(gesture.getIdentifier() instanceof SlideshowCommands){
			switch((SlideshowCommands)gesture.getIdentifier()){
				case Back:
					SlideshowClient.instance().goBack();
					break;
				case Flip:
					SlideshowClient.instance().flip();
					break;
				case Next:
					SlideshowClient.instance().nextPhoto();
					break;
				case Previous:
					SlideshowClient.instance().previousPhoto();
					break;
				case Refresh:
					SlideshowClient.instance().refresh();
					break;
				default:
					break;
			}
		}
	}
	/* **************************************************************************************/
	private void addGestures(GestureController gestureController) {
        gestureController.addGestures(new ChangeDirectionGesture(
        		SlideshowCommands.Flip, 
        		(long) (DEFAULT_DIRECTION_TRANSITION_TIME*1.5),
        		Direction.Front, Direction.Right, Direction.Front));

        gestureController.addGestures(new ChangeDirectionGesture(
        		SlideshowCommands.Next,
        		DEFAULT_DIRECTION_TRANSITION_TIME
        	  , Direction.Front, Direction.Up, Direction.Front));
        gestureController.addGestures(new ChangeDirectionGesture(
        		SlideshowCommands.Previous,
        		DEFAULT_DIRECTION_TRANSITION_TIME
        	  , Direction.Front, Direction.Down, Direction.Front));
        
        long transitionTime = (long) (DEFAULT_DIRECTION_TRANSITION_TIME*1.5);
        gestureController.addGestures(
        	new SequenceGesture(
        		SlideshowCommands.Back
        		,null
        	  , new ChangeDirectionGesture(1, transitionTime,  
        			  Direction.Front, Direction.Up, Direction.Back)
	  	  	  , new HoldDirectionGesture(2,400L, Direction.Back)
        	)
        );
	}
	private void removeGestures(GestureController gestureController) {
		for(SlideshowCommands command : SlideshowCommands.values()){
			gestureController.removeGestureListenerByIdentifier(command);
		}
	}
}
