package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller;

import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.GestureController;
import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.utils.OrientationViewLimiter;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
	private static final String FRAGMENT_TAG = "FRAGMENT_TAG";
	private static final boolean GESTURE_ENABLED = true;
	PlaceholderFragment fragment;

	private EditText serverUrl;
	
	private GestureController gestureController;
	private SlideshowGestureController slideshowGestureController;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		serverUrl = (EditText) findViewById(R.id.serverUrlEditText);
		if(serverUrl!= null){
			serverUrl.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View view) {

					view.setEnabled(true);
					view.setFocusableInTouchMode(true);
					view.requestFocus();
					InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					inputMethodManager.showSoftInput(view, 0);
				}
				
			});
			serverUrl.setOnEditorActionListener(new OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					v.setFocusable(false);
					SlideshowClient.instance().setServerUrl(v.getText().toString());
					return false;
				}
			});
		}
		
		if(!GESTURE_ENABLED){
			fragment = (PlaceholderFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
			if (savedInstanceState == null) {
				fragment = new PlaceholderFragment();
				getSupportFragmentManager().beginTransaction()
						.add(R.id.container, fragment, FRAGMENT_TAG).commit();
			}
		}
		else{
			OrientationViewLimiter.instance().setOrientationToDefault(this);
			gestureController = new GestureController();
			slideshowGestureController = new SlideshowGestureController();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(GESTURE_ENABLED){
			gestureController.start(this);
			slideshowGestureController.start(gestureController);
		}
	}
	@Override
	protected void onPause() {
		super.onPause();
		if(GESTURE_ENABLED){
			gestureController.stop();
			slideshowGestureController.stop();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onClickServerUrlText(View view){
		view.setEnabled(true);
		view.setFocusableInTouchMode(true);
		view.requestFocus();
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.showSoftInput(view, 0);
		
		Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show();
	}
	
	public void flipClick(View view){
		SlideshowClient.instance().flip();
	}
	public void backClick(View view){
		SlideshowClient.instance().goBack();
	}
	public void refreshClick(View view){
		SlideshowClient.instance().refresh();
	}
	public void nextClick(View view){
		SlideshowClient.instance().nextPhoto();
	}
	public void previousClick(View view){
		SlideshowClient.instance().previousPhoto();
	}

	
	public String getServerUrl(){
		if(serverUrl != null){
			return serverUrl.getText().toString();
		}
		return null;
	}
	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		
		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			
			return rootView;
		}
	}

}
