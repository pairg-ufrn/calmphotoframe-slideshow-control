package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller;

import java.io.IOException;
import java.net.URI;

import org.apache.http.Header;
import org.apache.http.HttpResponse;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.ResponseHandlerInterface;


public class SlideshowClient {
	
	public class DebugAsyncResponseHandler extends AsyncHttpResponseHandler {
		@Override
		public void onSuccess(int statusCode, Header[] headers, byte[] response) {
			Log.d(getClass().getName(), 
					"StatusCode: " + statusCode 
					+"; Headers:" + headers
					+ "; Response: " + response);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers, byte[] errResponse, Throwable cause) {
			Log.d(getClass().getName(), 
					"StatusCode: " + statusCode 
					+"; Headers:" + headers
					+ "; Response: " + errResponse);
		}
	}
	private static SlideshowClient singleton;
	
	public static SlideshowClient instance(){
		if(singleton == null){
			singleton = new SlideshowClient();
		}
		return singleton;
	}

	Http http;
	private String serverUrl;
	private AsyncHttpClient httpClient;
	
	public SlideshowClient() {
		serverUrl = "http://192.168.42.16:8888";
		http = new Http();
		
		httpClient = new AsyncHttpClient();
	}
	
	public void nextPhoto(){
//		http.get(getNextPhotoUrl());
		String url = getNextPhotoUrl();
		Log.d(getClass().getName(), "Next photo URL: " + url);
		
		httpClient.get(url, new DebugAsyncResponseHandler());
	}
	public void previousPhoto(){
		httpClient.get(getPreviousPhotoUrl(), new DebugAsyncResponseHandler());
	}
	public void flip(){
		httpClient.get(getFlipUrl(), new DebugAsyncResponseHandler());
	}
	public void refresh(){
		httpClient.get(getRefreshUrl(), new DebugAsyncResponseHandler());
	}
	public void goBack(){
		httpClient.get(getGoBackUrl(), new DebugAsyncResponseHandler());
	}

	public String getServerUrl(){
		return this.serverUrl;
	}
	public void setServerUrl(String url) {
		this.serverUrl = url;
	}
	
	private String getPreviousPhotoUrl() {
		return buildCommandUrl("previous");
	}
	private String getFlipUrl() {
		return buildCommandUrl("flip");
	}
	private String getRefreshUrl() {
		return buildCommandUrl("refresh");
	}
	private String getGoBackUrl() {
		return buildCommandUrl("back");
	}
	private String getNextPhotoUrl() {
		return buildCommandUrl("next");
	}
	private String buildCommandUrl(String commandName) {
		return getServerUrl() + "/commands/" + commandName;
	}

}
