package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller;

public interface Dispatcher {
	void execute(Runnable runnable);
}
