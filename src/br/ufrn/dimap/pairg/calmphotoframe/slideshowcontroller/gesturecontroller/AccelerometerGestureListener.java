package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller;

import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.gesture.AccelerometerGesture;

public interface AccelerometerGestureListener{
	void onGestureDetected(AccelerometerGesture gesture);
}