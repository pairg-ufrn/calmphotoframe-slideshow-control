package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.gesture;

import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.AccelerationEvent;
import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.Direction;
import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.DirectionDetector;


public class ChangeDirectionGesture extends TimedGesture{
	private Direction[] directionSequence;
	private int directionCount;

	public ChangeDirectionGesture(Object identifier, Direction ... directionSequence) {
		this(identifier,null,directionSequence);
	}
	public ChangeDirectionGesture(Object identifier, Long maxTimeInMillisBetweenChanges, Direction ... directionSequence) {
		super(identifier, maxTimeInMillisBetweenChanges);
		this.directionSequence = directionSequence;
	}

	@Override
	public void update(AccelerationEvent evt) {
		if(super.detected()){
			return;
		}
		if(super.timeOver()){
			this.reset();
		}
		
		Direction detectedDirection = DirectionDetector.instance().getUpDirection(evt);
		Direction directionToDetect = getCurrentDirection();
		if(directionToDetect !=null && detectedDirection == directionToDetect){
			++directionCount;
			super.restartCount();
			if(directionCount == directionSequence.length){ //Não tem mais o que detectar
				super.setCurrentState(GestureState.Detected);
				return;
			}
			//Se gesto terminou, ignora isto
			if(directionCount == 1){
				//Detectou a primeira direção, este é o ponto inicial para a detecção do gesto
				//o dispositivo pode se manter nesta direção por um tempo indeterminado
				setCurrentState(GestureState.Ready);
			}
			else if(directionCount == 2){
				//Após detectar segunda direção, considera-se que o movimento foi iniciado,
				//após isto, há um tempo limite para finalizar o gesto
				setCurrentState(GestureState.Started);
			}
		}
		else{//A direção atual não é a esperada, ela pode ser a mesma anterior ou ter se modificado
			Direction previousDirection = getPreviousDirection();
			if(previousDirection != null && detectedDirection != previousDirection){
				//Se mudou a direção para uma não esperada, reinicia gesto
				reset();
			}
		}
	}

	private Direction getPreviousDirection() {
		return directionCount > 0 ? this.directionSequence[directionCount - 1]
								  : null;
	}

	private Direction getCurrentDirection() {
		return directionSequence.length > 0 ? directionSequence[this.directionCount]
											: null;
	}

	@Override
	public void reset() {
		super.reset();
		this.directionCount = 0;
	}

}
