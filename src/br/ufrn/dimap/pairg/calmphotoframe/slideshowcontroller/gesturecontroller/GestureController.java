package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller;

import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.gesture.AccelerometerGesture;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Vibrator;

public class GestureController {
	private int sensorRate;
	private final AccelerometerController accelerometerController;
	private final AccelerometerGestureDetector gestureDetector;
	private SensorManager sensorManager;
	private boolean started;
	public GestureController() {
		accelerometerController = new AccelerometerController();
		gestureDetector = new AccelerometerGestureDetector();
		accelerometerController.addAccelerationListener(gestureDetector);
		this.sensorRate = SensorManager.SENSOR_DELAY_NORMAL;
		started = false;
	}
	
	public void addGestures(AccelerometerGesture ... gestures){
		for(AccelerometerGesture gesture : gestures){
			this.gestureDetector.addGesture(gesture);
		}
	}
	public void addGestureListener(AccelerometerGestureListener gestureListener) {
		this.gestureDetector.addGestureListener(gestureListener);
	}
	public void removeGestureListener(AccelerometerGestureListener gestureListener) {
		this.gestureDetector.removeGestureListener(gestureListener);
	}
	
	public int getSensorRate() {
		return sensorRate;
	}
	public void setSensorRate(int sensorRate) {
		this.sensorRate = sensorRate;
	}

	public boolean started(){
		return started;
	}
	Vibrator vibrator;
	public void start(Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if(accelerometer != null){
	        sensorManager.registerListener(accelerometerController,
	                accelerometer,
	                sensorRate);

	    	vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
	        gestureDetector.addGestureListener(new AccelerometerGestureListener() {
				@Override
				public void onGestureDetected(AccelerometerGesture gesture) {
					if(vibrator!=null){
						vibrator.vibrate(300);
					}
				}
			});
	        this.started = true;
        }
        else{
        	started = false;
        }
	}

	public void stop() {
		if(started()){
			sensorManager.unregisterListener(accelerometerController);
		}
	}

	public void removeGestureListenerByIdentifier(Object gestureIdentifier) {
		this.gestureDetector.removeGestureByIdentifier(gestureIdentifier);
	}

	
}
