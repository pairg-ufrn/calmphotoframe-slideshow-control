package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.gesture;

import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.AccelerationEvent;


public interface AccelerometerGesture {
	public enum GestureState{
		
		Idle(0),	   /** Ainda não reconheceu o ponto inicial do gesto*/
		Ready(1),      /** Detectou o ponto inicial do gesto*/
		Started(2),    /** Iniciou detecção*/
		Detected(3);   /** Concluiu detecção com sucesso*/;

		private final int order;
		
		private GestureState(int order) {
			this.order = order;
		}

		public boolean isSuccessor(GestureState other) {
			return this.order > other.order;
		}
		public boolean isAncestor(GestureState other) {
			return this.order < other.order;
		}
	}
	Object getIdentifier();
	
	void setIdentifier(Object identifier);
	
	void update(AccelerationEvent evt);

	GestureState getCurrentState();
	boolean detected();

	void reset();
}
