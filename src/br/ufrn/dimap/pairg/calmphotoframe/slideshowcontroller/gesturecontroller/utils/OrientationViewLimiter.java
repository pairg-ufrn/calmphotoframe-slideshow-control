package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.view.Surface;
import android.view.WindowManager;

public class OrientationViewLimiter {
	public enum Orientation{
		Portrait, Landscape, ReversePortrait, ReverseLandscape
	};
	
	private static OrientationViewLimiter singleton;
	public static OrientationViewLimiter instance() {
		if (singleton == null) {
			singleton = new OrientationViewLimiter();
		}
		return singleton;
	}
	
	/**
	 * Adaptado de: http://stackoverflow.com/a/9888357
	 * Encontrado em: http://stackoverflow.com/questions/4553650/how-to-check-device-natural-default-orientation-on-android-i-e-get-landscape 
	 * */
	public Orientation getDeviceDefaultOrientation(Context context) {
		WindowManager windowManager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);

		Configuration config = context.getResources().getConfiguration();

		int rotation = windowManager.getDefaultDisplay().getRotation();

		if (((rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) && config.orientation == Configuration.ORIENTATION_LANDSCAPE)
				|| ((rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) && config.orientation == Configuration.ORIENTATION_PORTRAIT)) {
			// return Configuration.ORIENTATION_LANDSCAPE;
			return Orientation.Landscape;
		} else {
			// return Configuration.ORIENTATION_PORTRAIT;
			return Orientation.Portrait;
		}
	}
	public void setOrientation(Activity activity, Orientation orientation){
		int orientationValue = getOrientationValue(orientation);
		activity.setRequestedOrientation(orientationValue);
	}
	public void setOrientationToDefault(Activity activity){
		int orientationValue = getOrientationValue(this.getDeviceDefaultOrientation(activity));
		activity.setRequestedOrientation(orientationValue);
	}
	public int getOrientationValue(Orientation orientation) {
		switch (orientation) {
		case Portrait:
			return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
		case Landscape:
			return ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
		case ReversePortrait:
			return ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
		case ReverseLandscape:
			return ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
		default:
			return -1;
		}
	}
}
