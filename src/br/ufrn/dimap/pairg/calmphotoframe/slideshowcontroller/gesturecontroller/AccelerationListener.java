package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller;

public interface AccelerationListener{
	public void onAcceleration(AccelerationEvent evt);
}