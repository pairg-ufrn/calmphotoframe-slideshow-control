package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.gesture.AccelerometerGesture;

public class AccelerometerGestureDetector implements AccelerationListener {
	private final Set<AccelerometerGesture> gestures;
	private final Set<AccelerometerGestureListener> gestureListeners;
	
	public AccelerometerGestureDetector() {
		gestures = new HashSet<>();
		gestureListeners = new HashSet<>();
	}
	
	@Override
	public void onAcceleration(AccelerationEvent evt) {
		List<AccelerometerGesture> detectedGestures = new LinkedList<>();
		
		for(AccelerometerGesture gesture : gestures){
			gesture.update(evt);
			if(gesture.detected()){
				detectedGestures.add(gesture);
			}
		}
		if(!detectedGestures.isEmpty()){
			AccelerometerGesture detected = choiceDetectedGesture(detectedGestures);
			notifyGestureDetected(detected);
			resetGestures();
		}
	}

	protected void resetGestures() {
		for(AccelerometerGesture gesture : this.gestures){
			gesture.reset();
		}
	}

	protected void notifyGestureDetected(AccelerometerGesture gesture) {
		for(AccelerometerGestureListener listener : this.gestureListeners){
			listener.onGestureDetected(gesture);
		}
	}

	protected AccelerometerGesture choiceDetectedGesture(List<AccelerometerGesture> detectedGestures) {
		//TODO: selecionar o gesto que começou antes
		return detectedGestures.get(0);
	}

	public void addGesture(AccelerometerGesture gesture) {
		this.gestures.add(gesture);
	}
	public void removeGesture(AccelerometerGesture gesture) {
		this.gestures.remove(gesture);
	}
	public void removeGestureByIdentifier(Object gestureIdentifier) {
		AccelerometerGesture gesture = findGestureByIdentifier(gestureIdentifier);
		if(gesture != null){
			this.removeGesture(gesture);
		}
	}
	public AccelerometerGesture findGestureByIdentifier(Object gestureIdentifier){
		for(AccelerometerGesture gesture : this.gestures){
			if(identifierIsEquals(gesture, gestureIdentifier)){
				return gesture;
			}
		}
		return null;
	}

	private boolean identifierIsEquals(AccelerometerGesture gesture,
			Object gestureIdentifier) {
		Object identifier = gesture.getIdentifier();
		if(identifier != null){
			return identifier.equals(gestureIdentifier);
		}
		return false;
	}

	public void addGestureListener(AccelerometerGestureListener accelerometerGestureListener) {
		this.gestureListeners.add(accelerometerGestureListener);
	}
	public void removeGestureListener(AccelerometerGestureListener accelerometerGestureListener) {
		this.gestureListeners.remove(accelerometerGestureListener);
	}

}
