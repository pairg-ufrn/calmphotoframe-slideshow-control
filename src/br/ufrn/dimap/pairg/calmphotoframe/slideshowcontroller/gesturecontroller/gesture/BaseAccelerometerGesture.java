package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.gesture;

import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.AccelerationEvent;


public abstract class BaseAccelerometerGesture implements AccelerometerGesture {
	private Object identifier;
	private GestureState currentState;
	
	public BaseAccelerometerGesture() {
		this(null);
	}
	public BaseAccelerometerGesture(Object identifier) {
		this.identifier = identifier;
		this.currentState = GestureState.Idle;
	}
	
	@Override
	public Object getIdentifier() {
		return identifier;
	}

	@Override
	public void setIdentifier(Object identifier) {
		this.identifier = identifier;
	}

	@Override
	public abstract void update(AccelerationEvent evt);

	@Override
	public GestureState getCurrentState() {
		return currentState;
	}
	protected void setCurrentState(GestureState state){
		this.currentState = state;
	}
	
	@Override
	public boolean detected() {
		return currentState == GestureState.Detected;
	}

	@Override
	public void reset() {
		currentState = GestureState.Idle;
	}

}
