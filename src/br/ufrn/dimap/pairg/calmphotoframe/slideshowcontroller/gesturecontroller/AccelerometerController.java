package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller;

import java.util.ArrayList;
import java.util.List;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

public class AccelerometerController implements SensorEventListener
{
    private static final float ALPHA = 0.7f;
    private static final int MIN_INITIAL_DATA = 10;
    
    private float[] gravity;
    private int dataCount;

    private final List<AccelerationListener> listeners; 
    
    public AccelerometerController()
    {
    	listeners = new ArrayList<>();
        gravity = new float[3];
        dataCount = 0;
    }

    public void addAccelerationListener(AccelerationListener listener){
    	this.listeners.add(listener);
    }
    public void removeAccelerationListener(AccelerationListener listener){
    	this.listeners.remove(listener);
    }

    @Override
    public void onSensorChanged(SensorEvent event)
    {
        float[] rawAccel = event.values.clone();
        float[] filteredValues = filterAcceleration(rawAccel[0],
                rawAccel[1],
                rawAccel[2]);
        
        // Precisa realizar um número de leituras inicial mínimo para valores serem válidos
        if (++dataCount >= MIN_INITIAL_DATA)
        {
            notifyListeners(event, filteredValues == null ? rawAccel.clone() : filteredValues);
        }
        
    }

    /**
     * This method derived from the Android documentation and is available under
     * the Apache 2.0 license.
     * 
     * @see http://developer.android.com/reference/android/hardware/SensorEvent.html
     */
    private float[] filterAcceleration(float x, float y, float z)
    {
        float[] filteredValues = new float[3];
        
        gravity[0] = ALPHA * gravity[0] + (1 - ALPHA) * x;
        gravity[1] = ALPHA * gravity[1] + (1 - ALPHA) * y;
        gravity[2] = ALPHA * gravity[2] + (1 - ALPHA) * z;

        filteredValues[0] = x - gravity[0];
        filteredValues[1] = y - gravity[1];
        filteredValues[2] = z - gravity[2];
        
        return filteredValues;
    }
    
	private void notifyListeners(SensorEvent evt, float[] linearAccel) {
		AccelerationEvent event = new AccelerationEvent(evt.timestamp, evt.values, gravity, linearAccel);
		for(AccelerationListener listener : listeners){
			listener.onAcceleration(event);
		}
	}

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {
        // Do nothing
    }
}
