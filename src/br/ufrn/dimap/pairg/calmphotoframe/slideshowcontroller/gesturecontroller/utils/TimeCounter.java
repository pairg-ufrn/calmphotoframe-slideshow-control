package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.utils;

public class TimeCounter {
	private long startTime;
	private long countTime;
	private boolean counting;
	
	public void startCount(long timeToCountInMillis){
		startTime = getCurrentTime();
		countTime = timeToCountInMillis;
		counting = true;
	}
	protected long getCurrentTime() {
		return System.currentTimeMillis();
	}
	public boolean isCounting(){
		return counting;
	}
	public boolean finishedCount(){
		if(isCounting()){
			boolean finished = getCurrentTime() >= startTime + countTime;
			if(finished){
				counting = false;
			}
			return finished;
		}
		return true;
	}
	public void stopCounting() {
		this.counting = false;
	}
}
