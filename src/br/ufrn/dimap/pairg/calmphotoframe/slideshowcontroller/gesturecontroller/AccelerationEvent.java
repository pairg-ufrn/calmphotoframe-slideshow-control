package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller;

public class AccelerationEvent{
	private final float[] rawAcceleration, gravity, linearAcceleration;
	private long timestamp;
	public AccelerationEvent(long timestamp, float[] accel, float[] gravity, float[] linearAccel) {
		this.rawAcceleration    = new float[3];
		this.gravity 			= new float[3];
		this.linearAcceleration = new float[3];
		this.timestamp = timestamp;
		
		for(int i=0; i < 3; ++i){
			this.rawAcceleration[i]    = accel[i];
			this.gravity[i]            = gravity[i];
			this.linearAcceleration[i] = linearAccel[i];
		}
	}
	
	public float[] getRawAcceleration() {
		return rawAcceleration;
	}
	public float[] getGravity() {
		return gravity;
	}
	public float[] getLinearAcceleration() {
		return linearAcceleration;
	}

	public long getTimestamp() {
		return timestamp;
	}
}