package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller;


public class DirectionDetector {
	private static DirectionDetector singleton;
	public static DirectionDetector instance() {
		if (singleton == null) {
			singleton = new DirectionDetector();
		}
		return singleton;
	}
	
	public Direction[] getAxis(AccelerationEvent evt){
		float gravity[] = evt.getGravity();
		return new Direction[]{
				isPositive(gravity[0])? Direction.Right : Direction.Left
			  , isPositive(gravity[1])? Direction.Up : Direction.Down
			  , isPositive(gravity[2])? Direction.Front : Direction.Back
		};
	}

	private boolean isPositive(float f) {
		return f >= 0;
	}

	public Direction getUpDirection(AccelerationEvent evt) {
		Direction[] axisDirections = getAxis(evt);
		int largerAxis = selectAxis(evt.getGravity());
		
		return axisDirections[largerAxis];
	}
	private int selectAxis(float[] gravity) {
		int axis =0;
		float largeValue = Math.abs(gravity[0]);
		for(int i=1; i <3; ++i){
			float currentValue = Math.abs(gravity[i]);
			if(currentValue > largeValue){
				largeValue = currentValue;
				axis = i;
			}
		}
		return axis;
	}
}
