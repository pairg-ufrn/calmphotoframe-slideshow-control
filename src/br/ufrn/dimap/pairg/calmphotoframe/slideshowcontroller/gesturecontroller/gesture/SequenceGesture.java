package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.gesture;

import android.util.Log;
import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.AccelerationEvent;

public class SequenceGesture extends TimedGesture{
	private AccelerometerGesture[] childGestures;
	private int gestureCounter;
	private GestureState previousGestureState;
	
	public SequenceGesture(Object identifier, Long maxTimeInMillisBetweenChanges,
			AccelerometerGesture ... childGestures) {
		super(identifier, maxTimeInMillisBetweenChanges);
		this.childGestures = childGestures.clone();
		gestureCounter = 0;
		previousGestureState = GestureState.Idle;
	}

	@Override
	public void update(AccelerationEvent evt) {
		if(this.detected() || getCurrentGesture() == null){
			return;
		}		
		if(super.timeOver()){
			this.reset();
		}

		AccelerometerGesture currentGesture = getCurrentGesture();
		currentGesture.update(evt);
		
		GestureState currentGestureState = currentGesture.getCurrentState();
		if(previousGestureState != currentGestureState)
		{
			Log.d(getClass().getName(), 
					currentGesture.getIdentifier() + " mudou de " +
							previousGestureState + " para "	+ currentGestureState);
			
			if(!currentGestureState.isSuccessor(previousGestureState)){
				Log.d(getClass().getName(), "Filho Regrediu");
				this.reset();
				return;
			}
			else if(getCurrentState() == GestureState.Started){
				Log.d(getClass().getName(), "Reiniciar contagem de tempo");
				this.restartCount();
			}
			
			if((getCurrentState() == GestureState.Idle || getCurrentState() == GestureState.Ready) 
					&& currentGestureState != GestureState.Idle
					&& getCurrentState().isAncestor(currentGestureState))
			{
				GestureState nextState = currentGestureState == GestureState.Ready 
						? GestureState.Ready
						: GestureState.Started;
				

				Log.d(getClass().getName(), "Mudando de estado global de " + getCurrentState()
						+ " para " + nextState);
				
				this.setCurrentState(nextState);
			}
			
			if(currentGestureState == GestureState.Detected){
				Log.d(getClass().getName(), "Detectou gesto " + gestureCounter + "º "
						+ "(" + getCurrentGesture().getIdentifier() + ")");
				
				++gestureCounter;
				super.restartCount();
				this.previousGestureState = GestureState.Idle;
				if (gestureCounter == this.childGestures.length) {//concluiu
					this.setCurrentState(GestureState.Detected);
				}
				
				return;
			}
		}
		previousGestureState = currentGestureState;
	}

	@Override
	public void reset() {
		super.reset();
		this.gestureCounter = 0;
		previousGestureState = GestureState.Idle;
		
		for(AccelerometerGesture child : this.childGestures){
			child.reset();
		}
	}
	
	protected AccelerometerGesture getCurrentGesture() {
		if(gestureCounter >=0 && gestureCounter < childGestures.length){
			return childGestures[gestureCounter];
		}
		return null;
	}

}
