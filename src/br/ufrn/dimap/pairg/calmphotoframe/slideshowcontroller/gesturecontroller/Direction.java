package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller;

public enum Direction {
	None, Up, Down, Left, Right, Front, Back
}
