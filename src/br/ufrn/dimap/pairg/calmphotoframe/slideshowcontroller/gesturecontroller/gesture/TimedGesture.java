package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.gesture;

import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.AccelerationEvent;
import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.utils.TimeCounter;


public abstract class TimedGesture extends BaseAccelerometerGesture{
	private Long maxTimeBetweenChanges;
	private final TimeCounter timeCounter;
	
	public TimedGesture(Object identifier, Long maxTimeInMillisBetweenChanges) {
		super(identifier);
		this.maxTimeBetweenChanges = maxTimeInMillisBetweenChanges;
		timeCounter = new TimeCounter();
	}

	public Long getMaxTimeBetweenChanges() {
		return maxTimeBetweenChanges;
	}
	public void setMaxTimeBetweenChanges(Long maxTimeBetweenChanges) {
		this.maxTimeBetweenChanges = maxTimeBetweenChanges;
	}

	@Override
	public abstract void update(AccelerationEvent evt);

	protected boolean timeOver() {
		if(maxTimeBetweenChanges != null
				&& timeCounter.finishedCount())
		{
			return true;
		}
		return false;
	}

	@Override
	public void reset() {
		super.reset();
		timeCounter.stopCounting();
	}
	protected void stopCount(){
		this.timeCounter.stopCounting();
	}
	protected void restartCount(){
		this.timeCounter.stopCounting();
		if(maxTimeBetweenChanges != null){
			this.timeCounter.startCount(maxTimeBetweenChanges);
		}
	}
	

}
