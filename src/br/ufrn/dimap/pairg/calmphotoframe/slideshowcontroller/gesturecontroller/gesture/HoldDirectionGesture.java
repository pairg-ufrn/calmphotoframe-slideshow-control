package br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.gesture;

import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.AccelerationEvent;
import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.Direction;
import br.ufrn.dimap.pairg.calmphotoframe.slideshowcontroller.gesturecontroller.DirectionDetector;

public class HoldDirectionGesture extends BaseAccelerometerGesture {
	private long timeToHold;
	private Direction direction;
	private long timestarted;
	
	public HoldDirectionGesture(Object identifier, long timeToHold, Direction dir) {
		super(identifier);
		this.direction = dir;
		this.timeToHold = timeToHold;
	}
	
	@Override
	public void update(AccelerationEvent evt) {
		if(detected()){
			return;
		}
		
		Direction currentDirection = DirectionDetector.instance().getUpDirection(evt);
		if(getCurrentState() == GestureState.Idle){ 
			if(currentDirection == direction){
				timestarted = System.currentTimeMillis();
				setCurrentState(GestureState.Started);
			}
		}
		else{
			if(currentDirection != direction){
				reset();
			}
			else if(System.currentTimeMillis() - timestarted > timeToHold){
				setCurrentState(GestureState.Detected);
			}
		}
	}
}
